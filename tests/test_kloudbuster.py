# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""
test_kloudbuster
----------------------------------

Tests for `kloudbuster` module.
"""
from kloudbuster.kb_config import KBConfig
from kloudbuster.kloudbuster import process_cli_args

def test_kbconfig_default():
    # verify that we load the defaulgt config properly
    kbcfg = KBConfig()
    kbcfg.update_configs()
    cfg = kbcfg.config_scale
    assert cfg.openrc_file is None
    assert cfg.vm_creation_concurrency == 5
    assert cfg.client.flavor.vcpus == 1

config_yaml = """
client:
    flavor:
        vcpus: 100
        ram: 2048
        disk: 0
        extra_specs:
            "hw:cpu_policy": dedicated
    storage_stage_configs:
        vm_count: 1
        target: 'volume'
        disk_size: 10
        io_file_size: 55
"""
def test_kbconfig_overide(stage_fs):
    config_fs = {
        'config.yaml': config_yaml
    }
    stage_fs('/tmp/kbtest', config_fs)
    # verify that config override is working
    args = ['-c', '/tmp/kbtest/config.yaml']
    process_cli_args(args)
    kbcfg = KBConfig()
    kbcfg.init_with_cli()
    kbcfg.update_configs()
    cfg = kbcfg.config_scale
    print(cfg.client.storage_stage_configs)
    assert cfg.client.flavor.vcpus == 100
    assert cfg.client.storage_stage_configs.io_file_size == 55
